package Practical.PZ1;
/**
 * Created by student on 5/24/2016.
 */
public class VarClass {
    public static byte varByte;
    private static short varShort;
    private static int varInt;
    private static long varLong;
    private static  float varFloat;
    private static double varDouble;
    private static  char varChar;
    private static boolean varBoolean;

    public static void notInitializedClassVar(){
        System.out.println(varByte);
        System.out.println(varShort);
        System.out.println(varInt);
        System.out.println(varLong);
        System.out.println(varFloat);
        System.out.println(varDouble);
        System.out.println(varChar);
        System.out.println(varBoolean);
    }

    public static void localVarMethod(){
        byte localByte = 127;
        short localShort = -32669;
        int localInt = 145767;
        long localLong = 654984561;
        float localFloat = 5877.47f;
        double localDouble = 47487.475;
        char localChar = 'F';
        boolean localBoolean = 1<23;
        System.out.println(localByte);
        System.out.println(localShort);
        System.out.println(localInt);
        System.out.println(localLong);
        System.out.println(localFloat);
        System.out.println(localDouble);
        System.out.println(localChar);
        System.out.println(localBoolean);
    }

    public static void varFloatTest(){
        float first = (float)1.;
        float second = 1f;
        float third = 0x1f;
        float fourth = (float)0b1;
        float fifth = 1.0e1f;
        float sixth = 01f;
        System.out.println(first);
        System.out.println(second);
        System.out.println(third);
        System.out.println(fourth);
        System.out.println(fifth);
        System.out.println(sixth);
    }

    public static void shortInicialized(){
        short first;
        short second;
        short third;
        short fourth;
        short fifth;
        float f1 = 67.5f;
        int i1 = 8477;
        double d1 = 47.4;
        System.out.println(first = 5+9);
        System.out.println(second = 5+(short)10.6);
        System.out.println(third = (short)(f1+i1));
        System.out.println(fourth = 0b0101011+68);
        System.out.println(fifth = (short)(f1+d1));
    }


}
