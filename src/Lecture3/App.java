package Lecture3;

/**
 * Created by HOME on 21.05.2016.
 */
public class App {

    private static final int MIN_NAME_LENGTH = 2;
    private static final int NORMA = 22;
    private static final int SALARY = 3001;

    public static String name;

    public static void main(String[] args) {
        System.out.println((1 > 2) && (3 < 5));
        System.out.println(method1(false) & method2(true));

        if(name != null && name.length() > MIN_NAME_LENGTH){
            System.out.println("Name: " + name);
        }

        System.out.println("true ^ false: " + (true^false));
        System.out.println("true ^ false: " + (false^false));
        System.out.println("true ^ true: " + (true^true));

        //Побитовые операции, побитовое И - & ,   ИЛИ - | исключающее побитовое ИЛИ и побитовое инвентирование

        int x = 5698;
        int y = 7852;
//*
        //Логическое И - &
        System.out.println("x   : " + Integer.toBinaryString(x));
        System.out.println("y   : " + Integer.toBinaryString(y));
        System.out.println("y&x : " + Integer.toBinaryString(x & y));

        System.out.println("      " + (x & y));

        //Логическое ИЛИ - |
        System.out.println("x   : " + Integer.toBinaryString(x));
        System.out.println("y   : " + Integer.toBinaryString(y));
        System.out.println("y|x : " + Integer.toBinaryString(x | y));

        System.out.println("      " + (x & y));

        //Логическое Исключающее ИЛИ - ^
        System.out.println("x   : " + Integer.toBinaryString(x));
        System.out.println("y   : " + Integer.toBinaryString(y));
        System.out.println("y^x : " + Integer.toBinaryString(x ^ y));

        System.out.println("      " + (x ^ y));

        //Инвентирование - ~
        System.out.println("~x : " + Integer.toBinaryString(~x));
        System.out.println("      " + (~x));

        //Побитовые сдвиги
        System.out.println("y    : " + (y));
        System.out.println("y<<2 : " + Integer.toBinaryString(y<<2));
        System.out.println("y<<2 : " + (y<<2));

        System.out.println("y    : " + (y));
        System.out.println("y>>2 : " + Integer.toBinaryString(y>>2));
        System.out.println("y>>2 : " + (y>>2));

        //Побитовый сдвиг с заполнителем 0 - >>>
        System.out.println("y    : " + (y));
        System.out.println("y>>>5 : " + Integer.toBinaryString(y>>>5));
        System.out.println("y>>>5 : " + (y>>>5));

        System.out.println();
        System.out.println();

        int workDays = 15;
        if (workDays > NORMA){
            System.out.println(SALARY * 2);
        } else {
            System.out.println(SALARY);
        }

        //тернарный оператор -  _?_:_/

        //(workDays>NORMA)? System.out.println(SALARY * 2):System.out.println(SALARY); - не заработает т.к. нет возвращаемого значнеия

        System.out.println((workDays>NORMA)? SALARY*2:SALARY);

        boolean result = (1<3) ? method1(false):method2(true);
        System.out.println(result);

        System.out.println();



        int i = 0;
        while (i<10) {
            System.out.println(i);
           i++;
        }



        /*вечный цикл
         int z = 0;
         while (true){
           System.out.println(i);
           z++;
        }
        */

        System.out.println();

        int j = 0;
        for (; j < 10; ){
            ++j;
            System.out.println(j);
            ++j;
        }
        System.out.println(j);

        System.out.println();




        m:  for (i = 0; i<5; i++){
            for(j = 0; j<7; j++){
                if(j==3){
                    continue m;
                }
                System.out.print("m" + i + j + " ");
            }
            System.out.println();
        }

        {
            int z = 5;
            System.out.println(z);
        }

        n:{
            System.out.println("A");
            {
                if(true){
                    break n;
                }
                System.out.println("B");
            }
            System.out.println("C");
        }





    }




    private static boolean method1(boolean b) {
        System.out.println("method1 (" + b + ")");
        return b;
    }

    private static boolean method2(boolean b) {
        System.out.println("method2 (" + b + ")");
        return b;
    }

    {



}
}