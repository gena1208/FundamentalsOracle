package Practical.PZ1;

import java.util.Scanner;
import javax.swing.*;


public class MainTestMethod {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Test for 1-st task");
        System.out.println("Примитивные типы данный класса");
        VarClass.notInitializedClassVar();
        System.out.println("Локальные примитивные типы данных");
        VarClass.localVarMethod();

        System.out.println();
        System.out.println("Test for 2-nd task");
        VarClass.varFloatTest();

        System.out.println();
        System.out.println("Test for 3-nd task");
        VarClass.shortInicialized();

        System.out.println();
        System.out.println("Test for 4-th task");
        SimpleDataTypeLoopsUnits.rightTriangle();

        System.out.println();
        System.out.println("Test for 5-th task");

        String num1  = JOptionPane.showInputDialog("Введите начальное число");
        int start = Integer.parseInt(num1);

        String num2  = JOptionPane.showInputDialog("Введите конечное число");
        int end = Integer.parseInt(num2);

        System.out.println("Start: " + start);
        System.out.println("End: " + end);

        int result = SimpleDataTypeLoopsUnits.intNumbersSumm(start, end);

        System.out.println("Summ of int numbers int rage from "
                + start + " to " + end + " is " + result);


        System.out.println();
        System.out.println("Test for 6-th task");
        String numEvenNums1  = JOptionPane.showInputDialog("Введите начальное число");
        int startEvenNums = Integer.parseInt(numEvenNums1);

        String numEvenNums2  = JOptionPane.showInputDialog("Введите конечное число");
        int endEvenNums = Integer.parseInt(numEvenNums2);

        System.out.println("Start: " + startEvenNums);
        System.out.println("End: " + endEvenNums);

        int resultEvenNums = SimpleDataTypeLoopsUnits.SummEvenNums(startEvenNums, endEvenNums);
        System.out.println("Сумма чептных чисел от " + startEvenNums +
                " до " + endEvenNums + " равно " + resultEvenNums);


        System.out.println();
        System.out.println("Test for 7-th task");
        SimpleDataTypeLoopsUnits.currentSummNum();



        System.out.println();
        System.out.println("Test for 8-th task");
        String inputNumA = JOptionPane.showInputDialog("Введите переменную А");
        int inputVarA = Integer.parseInt(inputNumA);

        String inputNumB = JOptionPane.showInputDialog("Введите переменную B");
        int inputVarB = Integer.parseInt(inputNumB);

        String inputNumC = JOptionPane.showInputDialog("Введите переменную C");
        int inputVarC = Integer.parseInt(inputNumC);

        SimpleDataTypeLoopsUnits.compareSumVar(inputVarA, inputVarB, inputVarC);

        System.out.println();
        System.out.println("Test for 9-th task");
        double valA = 651;
        double valB = 31;
        double resultCalcAcerageValue = SimpleDataTypeLoopsUnits.calcAverageValue(valA, valB);
        System.out.println("Среднее значение " + valA + " и " + valB + " равно " + resultCalcAcerageValue);

        System.out.println();
        System.out.println("Test for 10-th task");
        String inputLoanSum = JOptionPane.showInputDialog("Укажите сумму кредита");
        double inputVarLoanSum = Double.parseDouble(inputLoanSum);

        String inputPersent = JOptionPane.showInputDialog("Укажите процентную ставку");
        double inputVarPersent = Double.parseDouble(inputPersent);

        String inputLoanTerm = JOptionPane.showInputDialog("Укажите срок по кредиту в месяцах");
        int inputVarLoanTerm = Integer.parseInt(inputLoanTerm);

        SimpleDataTypeLoopsUnits.bankLoan(inputVarLoanSum,inputVarPersent,inputVarLoanTerm);
    }
}
