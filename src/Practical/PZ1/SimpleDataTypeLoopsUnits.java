package Practical.PZ1;


public class SimpleDataTypeLoopsUnits {
    public static int intNumbersSumm(int start, int end){
        int summ = 0;
        for(int i = start; i <=end; i++){
            summ+=i;
        }
        return summ;
    }


    /*4) Заданы значения 2-х катетов и гипотенузы треугольника. Проверить является ли
    данный треугольник прямоугольным. Вычисления записать выражением, состоящим из
    одной строки (используя оператор “?:”).*/
    public static void rightTriangle(){
        int a = 6;
        int b = 9;
        int c = 4;
        String triangle = ((a*a)+(b*b)==c*c)? "true":"false";
        System.out.println(triangle);
    }

    /*6) Подсчитать сумму четных целых цисел от 1 до 20.*/
    public static int SummEvenNums(int startEvenNums, int endEvenNums){
        int summEvenNums = 0;
        for(int i = startEvenNums; i <= endEvenNums; i++){
            if(i % 2 == 0){
                System.out.println("до " +i);
                summEvenNums += i;
                System.out.println("после " +i);
            }
        }
        return summEvenNums;
    }

    /*7) Подсчитать сумму простых чисел в диапазоне от 1 до 20.*/

    public static void currentSummNum(){
        int currentNumber, dividers;
        int summ = 0;
        for (currentNumber = 1; currentNumber < 21; currentNumber++){
            dividers = 0;
            for (int i = 1; i <= currentNumber; i++)
            {
                if (currentNumber % i == 0)
                    dividers++;
            }
            if (dividers <= 2){
               // System.out.println(currentNumber);
                summ+=currentNumber;
            }
        }
        System.out.println(summ);

    }


    /*8) Имея три переменных типа int a, b, c выведите на экран “true”, если сумма
    значений двух любых из этих переменных равна значению третьей.*/


    public static void compareSumVar (int a, int b, int c){
        if (a + b == c){
            System.out.println("a + b == c " + true);
        }if (a + c == b){
            System.out.println("a + c == b " + true);
        }if (b + c == a){
            System.out.println("b + c == a " + true);
        }else System.out.println("Равенство не обнаружено " + false);
    }

    /*9) Две переменные типа int, имеют положительные значения int a - начало
    диапазона, int b - конец диапазона, a > b. Вычислить среднее значение чисел в
    заданном диапазоне.*/

    public static double calcAverageValue(double a, double b){
        double result = 0;
        if (a>b){
            result=(a+b)/2;
            return result;
        }else
        System.out.println("Задан неверный диапазон чисел");
        return result;
    }

    /*
    * 10) Написать программу расчета обязательных платежей кредитора при
    пользовании банковским кредитом.
    Задание:
    а) Для каждого месяца вывести на экран сумму к оплате по телу кредита и
    начисленные проценты
    б) Суммарное значение выплаченных процентов за период пользования
    кредитом.
    Исходные данные: Сумма кредита, процентная ставка, срок кредитования.
    * */
    public static void bankLoan (double sumLoan, double percent, int loanTerm) {

        double percentPastMonth = (percent / loanTerm) / 100 + 1;
        double monthlySumOfLoan = sumLoan / loanTerm;
             double bodyOfLoan = sumLoan;
        double summCurrentPaymentPercent = 0;
        double sumCcurrentPaymentOfLoan = 0;
        double currentPaymentPercent = 0;
        double currentPaymentOfLoan = 0;

        for (int currentMonth = 1; currentMonth <= loanTerm; currentMonth++) {
            while (bodyOfLoan !=1 && bodyOfLoan>=1) {
                currentPaymentPercent = bodyOfLoan*percentPastMonth-bodyOfLoan;
                currentPaymentOfLoan = monthlySumOfLoan+currentPaymentPercent;
                System.out.println("Итого к выпплате по кредиту за текущий месяц " + currentPaymentOfLoan);
                System.out.println("из них сумма процентов " + currentPaymentPercent);
                bodyOfLoan = bodyOfLoan-monthlySumOfLoan;
                System.out.println("Текущее тело кредита " +bodyOfLoan);
                sumCcurrentPaymentOfLoan+=currentPaymentOfLoan;
                summCurrentPaymentPercent+=currentPaymentPercent;
            }
        }
        System.out.println("Выплачено процентов " + summCurrentPaymentPercent);
        System.out.println("Выплачено тела " + sumCcurrentPaymentOfLoan);
    }
}



